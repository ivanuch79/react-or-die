export const MODAL_CONTENT = new Map([
  [
    'first',
    {
      header: 'Its my first modal window',
      text: 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
      closeButton: true,
    },
  ],
  [
    'second',
    {
      header: 'Its my second modal window',
      text: 'is simply dummy text of the printing',
      closeButton: false,
    },
  ],
]);
