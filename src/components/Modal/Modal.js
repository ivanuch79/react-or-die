import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from 'components/Button';
import IconClose from 'components/IconClose';

import './Modal.scss';

class Modal extends Component {
  overlayClickHandler = e => {
    if (e.target.className === 'overlay') {
      this.props.onModalClosed();
    }
  };

  render() {
    const closeBtnStyles = {
      position: 'absolute',
      right: '8px',
      top: '8px',
    };

    return (
      <div className="overlay" onClick={this.overlayClickHandler}>
        <div className="modal">
          <div className="modal-header">{this.props.header}</div>
          <div className="modal-body">{this.props.text}</div>
          <div className="modal-actions">
            <Button appearance="outlined" text="Cancel" click={this.props.onModalClosed} />
            <Button text="Confirmed" styles={{ bgColor: '#ca0808' }} click={this.props.onModalClosed} />
          </div>
          <Button
            appearance="text"
            text={<IconClose />}
            styles={{
              display: this.props.closeButton ? 'flex' : 'none',
              ...closeBtnStyles,
            }}
            click={this.props.onModalClosed}
          >
            <IconClose styles={{ width: 14, color: 'grey', strokeWidth: 3 }} />
          </Button>
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  closeButton: true,
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
};

export default Modal;
