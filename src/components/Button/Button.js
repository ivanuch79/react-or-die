import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const BUTTON_APPEARANCE = {
  TEXT: 'text',
  OUTLINED: 'outlined',
  FILLED: 'filled',
};

class Button extends PureComponent {
  render() {
    const { classNames, appearance, color, size, radius, text, click, styles, leftIcon, rightIcon } = this.props;
    const defaultClass = 'btn';
    const classes = [defaultClass, size, appearance, ...classNames];

    let stylesList;

    switch (appearance) {
      case BUTTON_APPEARANCE.TEXT: {
        stylesList = {
          color,
          ...styles,
        };
        break;
      }
      case BUTTON_APPEARANCE.OUTLINED: {
        stylesList = {
          color,
          borderColor: color,
          borderRadius: radius,
          ...styles,
        };
        break;
      }
      case BUTTON_APPEARANCE.FILLED: {
        stylesList = {
          backgroundColor: color,
          borderRadius: radius,
          ...styles,
        };
        break;
      }
      default: {
        stylesList = {
          backgroundColor: color,
          borderRadius: radius,
          ...styles,
        };
      }
    }

    return (
      <button className={classes.join(' ')} style={stylesList} onClick={click}>
        {leftIcon} {text} {rightIcon}
      </button>
    );
  }
}

Button.defaultProps = {
  appearance: 'filled',
  size: 'default',
  color: '#333',
  classNames: [],
  radius: '4px',
};

Button.propTypes = {
  appearance: PropTypes.oneOf(['text', 'outlined', 'filled']),
  leftIcon: PropTypes.element,
  rightIcon: PropTypes.element,
  size: PropTypes.oneOf(['small', 'default', 'large']),
  radius: PropTypes.string,
  classNames: PropTypes.arrayOf(PropTypes.string),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  styles: PropTypes.shape(),
  click: PropTypes.func.isRequired,
};

export default Button;
