import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class IconClose extends PureComponent {
  render() {
    const { width, strokeWidth, color } = this.props.styles;
    const totalWidth = width + strokeWidth * 2;
    return (
      <svg width={totalWidth} height={totalWidth}>
        <line
          x1={strokeWidth}
          y1={totalWidth - strokeWidth}
          x2={totalWidth - strokeWidth}
          y2={strokeWidth}
          stroke={color}
          strokeWidth={strokeWidth}
        />
        <line
          x1={strokeWidth}
          y1={strokeWidth}
          x2={totalWidth - strokeWidth}
          y2={totalWidth - strokeWidth}
          stroke={color}
          strokeWidth={strokeWidth}
        />
      </svg>
    );
  }
}

IconClose.defaultProps = {
  styles: {
    width: 12,
    color: '#333',
    strokeWidth: 3,
  },
};

IconClose.propTypes = {
  styles: PropTypes.shape({
    width: PropTypes.number,
    color: PropTypes.string,
    strokeWidth: PropTypes.number,
  }),
};

export default IconClose;
