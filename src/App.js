import React, { Component } from 'react';

import Button from 'components/Button';
import ModalPortal from 'components/ModalPortal/ModalPortal';
import Modal from 'components/Modal';

import { MODAL_CONTENT } from './ModalContent';

import './App.scss';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      modalKey: null,
    };

    this.openFirstModal = this.openFirstModal.bind(this);
    this.openSecondModal = this.openSecondModal.bind(this);
  }

  openFirstModal() {
    this.setState({
      showModal: true,
      modalContent: MODAL_CONTENT.get('first'),
    });
  }

  openSecondModal() {
    this.setState({
      showModal: true,
      modalContent: MODAL_CONTENT.get('second'),
    });
  }

  onModalClosed = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    return (
      <div className="container">
        <div className="actions">
          <Button color="rgb(102, 187, 106)" text="Open first modal" click={this.openFirstModal} />
          <Button
            appearance="outlined"
            size="large"
            color="purple"
            text="Open second modal"
            click={this.openSecondModal}
          />
        </div>
        {this.state.showModal && (
          <ModalPortal>
            <Modal
              closeButton={this.state.modalContent.closeButton}
              header={this.state.modalContent.header}
              text={this.state.modalContent.text}
              onModalClosed={this.onModalClosed}
            />
          </ModalPortal>
        )}
      </div>
    );
  }
}
